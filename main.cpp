#include <signal.h>
#include <arpa/inet.h> // for inttypes

#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>

#include "LPS36.h"
#include "LPSMessage.h"


#define DATALENGTH 1024

//// Signal-safe flag for whether shutdown is requested
sig_atomic_t volatile requestShutdown = 0;

// Replacement SIGINT handler
void mySigIntHandler(int sig)
{
    // overriding the standard sigint handler, because ros::shutdown does not work
    // ros::shutdown is called before the return statement
    ROS_WARN("SIGINT... Shutting down");
    requestShutdown = 1;
    //  ros::shutdown();
}


int main(int argc, char **argv)
{
    // variables vor parameters
    std::string par_sensor_ip;
    int par_sensor_port;
    int par_exposure_duration;
    int par_sending_port;

    // variables used for message generation
    uint16_t lastScanNo = 0;
    int16_t xdata[DATALENGTH/2];
    uint16_t zdata[DATALENGTH/2];


    sensor_msgs::PointCloud pc;


    // init ros node
    ros::init(argc, argv, "lps36" ,ros::init_options::NoSigintHandler);

    ros::NodeHandle nh("~");
    nh.param("sensor_ip", par_sensor_ip , string("192.168.60.3"));
    nh.param("sensor_port", par_sensor_port, int(9008));
    nh.param("sending_port", par_sending_port, int(5634));
    nh.param("exposure_duration", par_exposure_duration, int(13109));
    if(par_exposure_duration > 13109 || par_exposure_duration<973)
    {
        ROS_ERROR("Exposure duration must be in range 973...13109. It is forced to 5000.");
        par_exposure_duration = 5000;
    }

    // set sigint handler (must be after creating node handle)
    signal(SIGINT, mySigIntHandler);

    // publish messages and buffer 1 message -> no buffer
    ros::Publisher pcPub = nh.advertise<sensor_msgs::PointCloud> ("pointcloud", 1);

    ros::Rate readRate(300); //300Hz read rate, so it is faster than the sending sensor, fames are send with 200 Hz (100 x + 100 y)/second

    if(ros::ok())
    {
        //init sensor

        LPS36 lps = LPS36(par_sensor_ip.c_str(), par_sending_port, par_sensor_port);
        if(lps.create())
        {
            ROS_INFO("Socket created!");
            ROS_INFO("Connecting to sensor %s:%d ...", par_sensor_ip.c_str(), par_sensor_port);
            if(lps.connect())
            {
                ROS_INFO("Connected to sensor!");
                if(lps.setExposureDuration(par_exposure_duration, false))
                {
                    ROS_INFO("Exposure duration set to %d", par_exposure_duration);
                    ROS_INFO("Publishing data ...");
                    //INIT of sensor is done: Publish data from now on
                    while(!requestShutdown)
                    {
                        if(lps.receive())
                        {                      

                            LPSMessage msg = lps.getMessage();
                            //ROS_INFO("New sensor frame received, datalength: %d", msg.getDataLength());

                            LPS::eData dataType = msg.getXZDataType();

                            //TODO: HANDLE XZ frames

                            if(dataType == LPS::LXS_DATA_X)
                            {
                                memcpy(xdata,msg.getData(),(2*msg.getDataLength()<DATALENGTH)? 2*msg.getDataLength() : DATALENGTH);
                            }
                            else if(dataType== LPS::LXS_DATA_Z)
                            {
                                memcpy(zdata,msg.getData(),(2*msg.getDataLength()<DATALENGTH)? 2*msg.getDataLength() : DATALENGTH);
                            }

                            if(lastScanNo==msg.getScanNo())
                            {
                                int length = msg.getDataLength();
                                pc.header.stamp = ros::Time::now();
                                pc.header.frame_id = "lps_frame";
                                pc.points.resize(length);
                                int i;
                                for(i = 0 ; i < length; i++)
                                {
                                    pc.points[i].x = xdata[i]/10000.;
                                    pc.points[i].y = 0;
                                    pc.points[i].z = zdata[i]/10000.;
                                }
                                pcPub.publish(pc);

                            }
                            else
                            {
                                lastScanNo = msg.getScanNo();
                            }
                            ros::spinOnce();
                            readRate.sleep();
                        }

                    }
                }
                else
                {
                    ROS_ERROR("Setting exposure durration failed!");
                }

                ROS_INFO("Disconnecting from sensor...");
                lps.disconnect();
                ROS_INFO("Disconnected");
            }
            else
            {
                ROS_ERROR("Connecting to sensor failed! Is Sensor in free run mode?");
            }
        }
        else
        {
            ROS_ERROR("Creation of socket failed! is socket allready in use?");
        }
    }

    ros::shutdown();

    return 0;
}
