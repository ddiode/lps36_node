#include "LPSMessage.h"

#include <cstddef> // needed for NULL
#include <cstring>
#include <iostream>

using namespace LPS;
using namespace std;


// standard constructor
LPSMessage::LPSMessage()
{
    data = NULL;
}


LPSMessage::LPSMessage(eCmd lpsCmd, eData XZDataType, uint16_t packetNo, uint16_t transactionNo, uint16_t status,
                uint32_t encoder, uint16_t scanNo, uint16_t dataLength, uint8_t* data)
{
    this->lpsCmd = lpsCmd;
    this->XZDataType = XZDataType;
    this->packetNo = packetNo;
    this->transactionNo = transactionNo;
    this->status = status;
    this->encoderVal = encoder;
    this->scanNo = scanNo;
    this->dataLength = dataLength;
    //if (dataLength == 0)
    //{
    //    this->data=NULL;
    //}
    //else
    //{
        this->data = new uint8_t[2*dataLength];
        memcpy(this->data, data, 2*dataLength);
    //}
}

/**
* Copy Constructor, important for data pointer handling
*/

LPSMessage::LPSMessage(const LPSMessage &msg)
{
    this->lpsCmd = msg.getLPSCmd();
    this->XZDataType = msg.getXZDataType();
    this->packetNo = msg.getPacketNo();
    this->transactionNo = msg.getTransactionNo();
    this->status = msg.getStateBin();
    this->encoderVal = msg.getEncoderVal();
    this->scanNo = msg.getScanNo();
    this->dataLength = msg.getDataLength();
    //if (msg.getDataLength() == 0)
    //{
    //    this->data=NULL;
    //}
    //else
    //{
        this->data = new uint8_t[2*this->dataLength];
        memcpy(this->data, msg.getData(), 2*dataLength);
    //}
}


LPSMessage::~LPSMessage()
{
    delete[] data;
}

// standard getters
eCmd LPSMessage::getLPSCmd() const
{
    return lpsCmd;
}

eData LPSMessage::getXZDataType() const
{
    return XZDataType;
}

uint16_t LPSMessage::getPacketNo() const
{
    return packetNo;
}

uint16_t LPSMessage::getTransactionNo() const
{
    return transactionNo;
}

uint32_t LPSMessage::getEncoderVal() const
{
    return encoderVal;
}

uint16_t LPSMessage::getScanNo() const
{
    return scanNo;
}

uint16_t LPSMessage::getDataLength() const
{
    return dataLength;
}

uint8_t* LPSMessage::getData() const
{
    return data;
}

uint16_t* LPSMessage::getData16() const
{
    return (uint16_t *) data;
}

LXSState LPSMessage::getState() const
{
    LXSState state;

    state.connectedViaEthernet = (this->status & 0x0001) > 0;
    switch( ((this->status) >> 4) & 0x000F) // taking only bit 4 to 8
    {
        case 0b0001: state.mode = LXS_MODE_MEASURE; break;
        case 0b0010: state.mode = LXS_MODE_MENU; break;
        case 0b0100: state.mode = LXS_MODE_COMMAND; break;
        case 0b1000: state.mode = LXS_MODE_ERROR; break;
        default: state.mode = LXS_MODE_INVALID;
    }
    state.activatedViaActivationFunction = (this->status & 0x0100) > 0;
    state.warningTemporarySensorMalfunction = (this->status & 0x0200) > 0;
    state.triggeredMeassureMode = (this->status & 0x0400) > 0;
    state.configurationMemoryConnected = (this->status & 0x0800) > 0;
    state.error = (this->status & 0x2000) > 0;

    return state;
}

uint16_t LPSMessage::getStateBin() const
{
    return status;
}
