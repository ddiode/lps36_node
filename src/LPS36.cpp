#include "LPS36.h"
#include "lpsDef.h"
#include "LPSMessage.h"
#include <iostream>
#include <string>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

using namespace LPS;

LPS36::LPS36()
{
    sensorIp = "192.168.60.3";
    rcvPort = 5634;
    sendPort = 9008;
    connected = false;
    created = true;
    // init addr structures
    memset((char *) &rcvAddr, 0, sizeof(rcvAddr));
    memset((char *) &sendAddr, 0, sizeof(sendAddr));

    // init receive and send buffers
    memset(rcvBuf, 0, RCVBUFSIZE);
    memset(sendBuf, 0, SENDBUFSIZE);
    rcvBuf16 = (uint16_t *) rcvBuf;
    sendBuf16 =(uint16_t *) sendBuf;
    rcvBufMess = NULL;
}

LPS36::LPS36(string sensorIp, int rcvPort, int sendPort)
{
    this->sensorIp = sensorIp;
    this->rcvPort = rcvPort;
    this->sendPort = sendPort;
    connected = false;
    created = true;
    // init addr structures
    memset((char *) &rcvAddr, 0, sizeof(rcvAddr));
    memset((char *) &sendAddr, 0, sizeof(sendAddr));

    // init receive and send buffers
    memset(rcvBuf, 0, RCVBUFSIZE);
    memset(sendBuf, 0, SENDBUFSIZE);
    rcvBuf16 = (uint16_t *) rcvBuf;
    sendBuf16 =(uint16_t *) sendBuf;
    rcvBufMess = NULL;
}

/**
 * TODO: close sockets
 */

LPS36::~LPS36()
{
    //close(sendSocket);
    //close(rcvSocket);
    delete rcvBufMess;
}

/**
* bool LPS36::create()
* crates sockets
*/
bool LPS36::create()
{
    // create receive socket
    rcvAddr.sin_family=AF_INET;
    rcvAddr.sin_port=htons(rcvPort); // htons converts from host to netwokr byte order
    //rcvAddr.sin_addr.s_addr=inet_addr("192.168.60.3");
    rcvAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    socklen_t addrLen = sizeof(struct sockaddr);
    rcvSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    struct timeval timeout;
    timeout.tv_sec = SOCKETTIMEOUTSEC;
    timeout.tv_usec = 0;

    if (setsockopt(rcvSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
    {
        // setting socket timeout failed
        return false;
    }

    if (rcvSocket < 0 )
    {
        //cout << "could not open receiving socket" << endl;
        created = false;
        return false;
    }

    if ( bind(rcvSocket,(sockaddr *) &rcvAddr,addrLen) <0 )
    {
        //cout << "bind receiving socket failed" << endl;
        created = false;
        return false;
    }

    // create send socket
    sendAddr.sin_family=AF_INET;
    sendAddr.sin_port=htons(sendPort); // htons converts from host to netwokr byte order
    sendAddr.sin_addr.s_addr=inet_addr("192.168.60.3");
    //sendAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    sendSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (sendSocket < 0)
    {
        //cout << "could not open sending socket" << endl;
        created = false;
        return false;
    }
    else
    {
        //cout << "sending socket created" << endl;
    }

    created = true;
    return true;
}


/**
* receive frames and parse them
* TODO: should be protected and in a seperate thread in future
* FIX: implement receiving ack frames
*/
bool LPS36::receive()
{
    ssize_t numBytes;

    if (created == false)
    {
        return false;
    }

    // can not check if connected because routine is needed for connecting
    //if (connected == false)
    //{
    //    return false;
    //}

    //cout << "waiting to receive"<<endl;

    // sensor Adress for filtering packages
    struct sockaddr_in rcvAddr2;
    //rcvAddr2.sin_family=AF_INET;
    //rcvAddr2.sin_port=htons(rcvPort); // htons converts from host to netwokr byte order
    //rcvAddr2.sin_addr.s_addr=inet_addr("192.168.60.3");
    //rcvAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    socklen_t addrLen = sizeof(struct sockaddr);
    numBytes = recvfrom(rcvSocket, rcvBuf, RCVBUFSIZE, 0,(struct sockaddr*)&rcvAddr2, &addrLen);
    if (numBytes == -1)
    {
        return false;
    }

    //cout << inet_ntoa(rcvAddr2.sin_addr);// << " vs. " << inet_addr(sensorIp.c_str()) << endl;

    if ( rcvAddr2.sin_addr.s_addr == inet_addr(sensorIp.c_str())) // check if sending ip is correct
    {
        //cout << numBytes << " bytes received from " <<inet_ntoa(rcvAddr2.sin_addr) << ":" <<ntohs(rcvAddr2.sin_port) << endl;

        if (numBytes >= 30) // LxS-device can send only packages >= 30 Bytes !
        {
//            size_t lenFrame = numBytes/2;

            // LPS FRAME:

            //--------------
            // Startseq 1
            // 0xFFFF
            //--------------
            // Startseq 2
            // 0xFFFF
            //--------------
            // Fill Char.
            // 0x0000
            //--------------
            // Command no.
            // 2 Byte
            //--------------
            // Fill Char
            // 0x0000
            //--------------
            // Packet No.
            // 2 Byte
            //--------------
            // Fill Char
            // 0x0000
            //--------------
            // Transaction Nr.
            // 2 Byte
            // allways 0x0000 in measurement mode
            //--------------
            // Status
            // 2 Byte
            //--------------
            // Encoder H + L
            // 4 Byte
            // Ox0000 00000 if no encoer is connected
            //--------------
            // Fill char
            // 0x0000
            //--------------
            // Scan No.
            // 2 Byte
            //--------------
            // Type
            // 0x0010 fixed
            //--------------
            // Number of user
            // data words
            // 2 Byte
            //--------------



            //swap big to little endian (should be architecture invariant)
            //for(unsigned int i=0; i < lenFrame; i++)
            //{
            //    rcvBuf16[i] = ntohs(rcvBuf16[i]);
            //}
            // parse Frame

            if (rcvBuf16[0] == LXS_HEAD_STARTSEQ1 && rcvBuf16[1] == LXS_HEAD_STARTSEQ2 && // check startseq
                rcvBuf16[2] == LXS_HEAD_FILLCHAR && rcvBuf16[4] == LXS_HEAD_FILLCHAR && //check fill chars
                rcvBuf16[6] == LXS_HEAD_FILLCHAR && rcvBuf16[11] == LXS_HEAD_FILLCHAR &&
                rcvBuf16[13] == LXS_HEAD_TYPE) // check frame type
            {
                // cout << "lxsframe!!"<< endl;

                // Xdata, Zdata, XZdata frame
                if(rcvBuf16[3] == LXS_RESP_DATA_X || rcvBuf16[3] == LXS_RESP_XZ_HIGH_DATA || rcvBuf16[3] == LXS_RESP_DATA_Z )
                {
                    if (rcvBuf16[14] == 0x0178) // check data length
                    {
                        eCmd lpsCmd = (eCmd)rcvBuf16[3];
                        eData XZDataType;
                        switch(rcvBuf16[3])
                        {
                            case LXS_RESP_DATA_X: XZDataType = LXS_DATA_X; break;
                            case LXS_RESP_XZ_HIGH_DATA: XZDataType = LXS_DATA_ZX; break;
                            case LXS_RESP_DATA_Z: XZDataType = LXS_DATA_Z; break;
                        }

                        uint16_t packetNo = rcvBuf16[5];

                        uint16_t transactionNo = rcvBuf16[7]; // is allways zero in measurement mode
                        uint16_t status = rcvBuf16[8];
                        uint32_t encoderVal = (rcvBuf16[9] << 16) | rcvBuf16[10]; // shifting high bytes left and or-ing low bytes in
                        uint16_t scanNo = rcvBuf16[12]; // number of profiles scanned, is the same value for x and z data, increases with every scanned line
                        uint16_t dataLength = rcvBuf16[14];


                        delete rcvBufMess; // delete old message
                        rcvBufMess = new LPSMessage(lpsCmd, XZDataType, packetNo, transactionNo, status,
                                            encoderVal, scanNo, dataLength, (uint8_t *)&rcvBuf16[15]);


                        return true; // frame parsed
                    }
                    else
                    {
                        return false;
                    }
                }

                // response to command frame
                else if(rcvBuf16[3] == LXS_RESP_ACK_SUCCESS || rcvBuf16[3] == LXS_RESP_ACK_FAILURE)
                {
                    // cout << "command response frame" << endl;
                    if (rcvBuf16[14] == 0x0000 || 0x0001 | 0x0002) // check data length
                    {
                        eCmd lpsCmd = (eCmd)rcvBuf16[3];
                        eData XZDataType = LXS_DATA_UNKNOWN;

                        uint16_t packetNo = rcvBuf16[5];

                        uint16_t transactionNo = rcvBuf16[7];
                        uint16_t status = rcvBuf16[8];
                        uint32_t encoderVal = (rcvBuf16[9] << 16) | rcvBuf16[10]; // shifting high bytes left and or-ing low bytes in
                        uint16_t scanNo = rcvBuf16[12]; // number of profiles scanned, is the same value for x and z data, increases with every scanned line
                        uint16_t dataLength = rcvBuf16[14];

                        delete rcvBufMess; // delete old message
                        rcvBufMess = new LPSMessage(lpsCmd, XZDataType, packetNo, transactionNo, status,
                                            encoderVal, scanNo, dataLength, (uint8_t *)&rcvBuf16[15]);


                        return true; // frame parsed
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            else
                return false;
        }
        else
            return false;

    }


    return false;
}

/**
* sets Exposure time -> higher is better for darker commands
* duration must be between 973 & 13109 duration value is in 1/10 ms
* FIX: think about connection handling
*/
bool LPS36::setExposureDuration(uint16_t duration, bool permanent)
{
    if (duration < 973 || duration > 13109)
    {
        return false; // duration not allowed
    }

    if(sendCommand(LXS_CMD_ENTER_COMMAND_MODE,0,NULL))
    {
        // cout << "enter cmd mode succesful" << endl;
        // think about connection handling
        //connected=false;
        uint8_t data[6];
        uint16_t *data16 = (uint16_t *)data;

        if(permanent) // set save flag
        {
            data16[0] = 0x0001;
        }
        else
        {
            data16[0] = 0x0000;
        }

        data16[1] = 0x0BBE; // manual adjustment of exposure data

        data16[2] = duration; // it is already checked if duration is valid

        if(sendCommand(LXS_CMD_SET_SINGLE_INSPECTION_TASK_PARAMETER, 6, data))
        {
            // cout << "sending exposure time" << endl;
            if(sendCommand(LXS_CMD_EXIT_COMMAND_MODE,0,NULL))
            {
                // cout << "exited command mode sucessfully" << endl;
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            if(sendCommand(LXS_CMD_EXIT_COMMAND_MODE,0,NULL))
            {
                // cout << "exited command mode sucessfully" << endl;
                return false;
            }
            else
            {
                return false;
            }
            return false;
        }
    }
    return false;
}

/**
* sends connect command
*
*/
bool LPS36::connect()
{
    if(sendCommand(LXS_CMD_CONNECT_TO_SENSOR,0,NULL))
    {
        connected = true;
        return true;
    }
    else
    {
        return false;
    }
}


/**
* sends disconnect command
*
*/
bool LPS36::disconnect()
{
    if(sendCommand(LXS_CMD_DISCONNECT_FROM_SENSOR ,0,NULL))
    {
        connected = false;
        return true;
    }
    else
    {
        return false;
    }
}


/**
* sends command to sensor, and checks if answer is ACK_SUCCESS
* TODO: implement stopping thread when receiving package
*/
bool LPS36::sendCommand(eCmd lpsCmd, uint16_t dataLength, uint8_t* data)
{
    eData XZDataType = LXS_DATA_UNKNOWN;
    uint16_t packetNo = 0;
    uint16_t transactionNo = 0;
    uint16_t status = 0;
    uint32_t encoder = 0;
    uint16_t scanNo = 0;

    LPSMessage msg(lpsCmd, XZDataType, packetNo, transactionNo, status, encoder, scanNo, dataLength, data);

    if(isCreated())
    {
        sendMessage(&msg);

        for(int i = 1; i < MAXWAITFORACK; i++)
        {
            if(receive())
            {
                if(rcvBufMess->getTransactionNo() == lpsCmd &&
                    rcvBufMess->getLPSCmd() == LXS_RESP_ACK_SUCCESS)
                {
                    return true; // ack received
                }
                else if (rcvBufMess->getTransactionNo() == lpsCmd &&
                    rcvBufMess->getLPSCmd() == LXS_RESP_ACK_FAILURE)
                {
                    return false; // nack received
                }
            }
            else
            {
                return false; // receiving error
            }
        }
        return false; // no correct data received
    }
    else
    {
        return false; // sockets not created
    }
    return false;
}


/**
* Getter for connected flag
*/
bool LPS36::isConnected()
{
    return connected;
}

/**
* Getter for created flag
*/
bool LPS36::isCreated()
{
    return created;
}

LPSMessage LPS36::getMessage()
{
    return *rcvBufMess;
}


/**
* Sends a low level message to sensor
*/
bool LPS36::sendMessage(LPSMessage *msg)
{
    size_t sendLen;

    sendBuf16[0] = LXS_HEAD_STARTSEQ1;
    sendBuf16[1] = LXS_HEAD_STARTSEQ2; // startseq, fixed value
    sendBuf16[2] = LXS_HEAD_FILLCHAR; // fixed value
    sendBuf16[3] = (uint16_t) msg->getLPSCmd();
    sendBuf16[4] = LXS_HEAD_FILLCHAR;
    sendBuf16[5] = msg->getPacketNo();
    sendBuf16[6] = LXS_HEAD_FILLCHAR; // fixed value
    sendBuf16[7] = msg->getTransactionNo();
    sendBuf16[8] = msg->getStateBin();
    sendBuf16[9] = (uint16_t) ((msg->getEncoderVal() >> 16) & 0xffff);// upper two bytes of encoder val
    sendBuf16[10] = (uint16_t) (msg->getEncoderVal() & 0xffff);       // lower two bytes of encoder val
    sendBuf16[11] = LXS_HEAD_FILLCHAR; // fixed value
    sendBuf16[12] = msg->getScanNo();
    sendBuf16[13] = LXS_HEAD_TYPE; // frame type, fixed value
    sendBuf16[14] = msg->getDataLength();

    if(msg->getDataLength() < SENDBUFSIZE - 30)
    {
        memcpy(&sendBuf16[15], msg->getData(), (msg->getDataLength() < SENDBUFSIZE - 30) ? msg->getDataLength() : SENDBUFSIZE -30 );
    }
    else
    {
        return false;
    }

    sendLen = 30+msg->getDataLength(); // length of data + header

    socklen_t addrLen = sizeof(struct sockaddr);
    if ( sendto(sendSocket, sendBuf, sendLen , 0 , (struct sockaddr *) &sendAddr, addrLen) == -1 )
    {
        cout << "sending failed" << endl;
        return false;
    }
    else
    {
        return true;
    }

    return false;
}


/**
*
*
*/
