#ifndef LPS36_H
#define LPS36_H

#include <string>
#include <sys/socket.h>
#include <netdb.h>
#include "LPSMessage.h"

using namespace std;

#define RCVBUFSIZE 1024 // receiving buffer size in byte, 1024 is enough for every valid data frame
#define SENDBUFSIZE 1024
#define MAXWAITFORACK 16 // listen for n frames for ack after sending a command
#define SOCKETTIMEOUTSEC 1 // socket timeout for receiving operations

/**
*
*/

class LPS36
{
    public:
        LPS36();
        LPS36(string sensorIp, int rcvPort,int sendPort);
        virtual ~LPS36();

        // high level routines
        bool connect();
        bool disconnect();
        bool create();
        bool receive();
        bool setExposureDuration(uint16_t duration, bool permanent);

        // low level routines
        bool sendCommand(eCmd lpsCmd, uint16_t dataLength, uint8_t* data);

        // getters
        bool isConnected();
        bool isCreated();
        LPSMessage getMessage();

    protected:
        // low level commands
        bool sendMessage(LPSMessage *msg);

        // network Stuff
        uint8_t rcvBuf[RCVBUFSIZE];
        uint16_t *rcvBuf16;
        uint8_t sendBuf[SENDBUFSIZE];
        uint16_t *sendBuf16;

        //uint16_t status;
        string sensorIp;
        int rcvPort;
        int rcvSocket;
        struct sockaddr_in rcvAddr;
        int sendPort;
        int sendSocket;
        struct sockaddr_in sendAddr;
        LPSMessage *rcvBufMess;

        // internal flags
        bool connected;
        bool created;


    private:

};

#endif // LPS36_H
