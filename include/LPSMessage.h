#ifndef LPSMESSAGE_H
#define LPSMESSAGE_H

#include "lpsDef.h"
#include <arpa/inet.h> // for inttypes definitons



using namespace LPS;
using namespace std;

class LPSMessage
{
    public:
        // con/de-structors
        LPSMessage();
        LPSMessage(eCmd lpsCmd, eData XZDataType, uint16_t packetNo, uint16_t transactionNo, uint16_t status,
            uint32_t encoder, uint16_t scanNo, uint16_t dataLength, uint8_t* data);
        LPSMessage(const LPSMessage &msg); //copy constructor
        virtual ~LPSMessage();

        // getters and setters
        eCmd getLPSCmd() const;
        eData getXZDataType() const;
        uint16_t getPacketNo() const;
        uint16_t getTransactionNo() const;
        uint32_t getEncoderVal() const;
        uint16_t getScanNo() const;
        uint16_t getDataLength() const;
        uint8_t* getData() const;
        uint16_t* getData16() const;
        LXSState getState() const;
        uint16_t getStateBin() const;
    protected:
        eCmd lpsCmd;
        eData XZDataType;
        uint16_t packetNo;
        uint16_t transactionNo;
        uint16_t status;
        uint32_t encoderVal;
        uint16_t scanNo;
        uint16_t dataLength;
        uint8_t* data;
    private:
};

#endif // LPSMESSAGE_H
